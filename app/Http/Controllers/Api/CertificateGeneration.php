<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CertificateStoreRequest;
use PDF;
use Mail;

class CertificateGeneration extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(),[
            //'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'variables' => 'required|json',
            'template'  =>  'required|integer',
        ]);

        if($request->token !== env('API_TOKEN')){
            return response()->json([
                'message' => 'You cannot sign with those credentials',
                'errors' => 'Unauthorised'
            ], 401);
        }
        
        if($validator->fails()){
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        $toEmail = $request->email;
        $template = $request->template;
        $variables = json_decode($request->variables, true);

        $pdf = PDF::loadView($template, $variables);

        $mailData = [
            'subject'   =>  env('MAIL_SUBJECT'),
            'toEmail'   =>  $toEmail,
            'file'  =>  $pdf,
            'certificateNumber' =>  $variables['certificateNumber'],
        ];

        $htmlLetter = view('htmlEmail', []);

        Mail::html($htmlLetter, function ($message) use ($mailData)
        { 
            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $message->to($mailData['toEmail']);
            $message->subject($mailData['subject']);
            $message->attachData($mailData['file']->output(), $mailData['certificateNumber'] . '.pdf');
        });

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}